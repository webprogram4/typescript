let w: unknown = 1;
w = "string";
w = {
    runANonExistenetMethod:() =>{
        console.log("I think therefor I am");
    }
} as { runANonExistenetMethod:() => void }

if(typeof w == 'object' && w!== null){
    (w as {runANonExistenetMethod: Function}).runANonExistenetMethod();
}